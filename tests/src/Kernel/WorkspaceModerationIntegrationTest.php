<?php

namespace Drupal\Tests\workspace_moderation\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\workspaces\Kernel\WorkspaceTestTrait;
use Drupal\workspaces\Entity\Workspace;

/**
 * Test the workspace moderation.
 *
 * @group workspace_moderation
 */
class WorkspaceModerationIntegrationTest extends KernelTestBase {

  use ContentTypeCreationTrait;
  use ContentModerationTestTrait;
  use NodeCreationTrait;
  use UserCreationTrait;
  use WorkspaceTestTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The workspaces moderation manager service.
   *
   * @var \Drupal\workspace_moderation\WorkspaceModerationManagerInterface
   */
  protected $workspaceModerationManager;

  /**
   * The checked node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The workspace moderator account.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $workspaceModerator;

  /**
   * An account that can see the workspace content.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $workspaceViewer;

  protected static $modules = [
    'content_moderation',
    'field',
    'filter',
    'node',
    'text',
    'user',
    'system',
    'language',
    'workflows',
    'workspaces',
    'workspace_moderation',
  ];

  protected function setUp() {
    parent::setUp();

    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->nodeStorage =
      $this->entityTypeManager->getStorage('node');
    $this->workspaceModerationManager =
      \Drupal::service('workspaces.manager');

    $this->installEntitySchema('content_moderation_state');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('workspace');

    $this->installConfig(['filter', 'node', 'system', 'language']);

    $this->installSchema('node', ['node_access']);
    $this->installSchema('system', ['key_value_expire', 'sequences']);
    $this->installSchema('workspaces', ['workspace_association']);

    $this->createContentType(['type' => 'page']);

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->save();

    Workspace::create(['id' => 'stage', 'label' => 'Stage'])->save();

    $this->workspaceModerator = $this->createUser([
      'access moderated workspaces',
      'administer nodes',
      'administer workspaces',
    ]);
    $this->workspaceViewer = $this->createUser([
      'view any workspace',
    ]);
  }

  public function testShadowWorkspaces() {
    $this->setCurrentUser($this->workspaceModerator);

    // Create the node on Live.
    $this->node = $this->createNode([
      'title' => 'Live - initial - published',
      'status' => TRUE
    ]);

    // Switch to stage and check if the shadow workspaces has kicked in.
    $this->switchWorkspace('stage');
    $this->assertActiveWorkspaceId(
      'stage-moderated',
      'A user with the "access moderated workspaces" permission is switched to the shadow workspace.'
    );
    $this->assertNodeTitle(
      'Live - initial - published',
      'The initial title on stage is the same as on live.'
    );

    // Save a new draft and verify that it's the default revision on the shadow
    // workspace.
    $this->node->setTitle('Stage - shadow - draft');
    $this->node->moderation_state = 'draft';
    $this->node->save();
    $this->reloadNode();
    $this->assertNodeTitle(
      'Stage - shadow - draft',
      'The shadow workspace shows the latest draft.'
    );

    // Log in as the user without the "access moderated workspaces" permission
    // and verify that they can't see the draft version.
    $this->setCurrentUser($this->workspaceViewer);
    $this->switchWorkspace('stage');
    $this->assertActiveWorkspaceId(
      'stage',
      'A user without the "access moderated workspaces" permission is switched to the regular workspace.'
    );
    $this->assertNodeTitle(
      'Live - initial - published',
      'A draft from the shadow workspace is not populated to the main workspace.'
    );

    // Save the node in a default revision state (published) and verify that
    // the changes have propagated the real workspace.
    $this->setCurrentUser($this->workspaceModerator);
    $this->switchWorkspace('stage');
    $this->node->setTitle('Stage - shadow - published');
    $this->node->moderation_state = 'published';
    $this->node->save();

    $this->setCurrentUser($this->workspaceViewer);
    $this->switchWorkspace('stage');
    $this->assertNodeTitle(
      'Stage - shadow - published',
      'A default revision propagates to the main workspace.'
    );
  }

  /**
   * Asserts the active workspace's id.
   *
   * @param string $expectedId
   *   The expected value.
   * @param string $message
   *   A message describing the assertion.
   */
  protected function assertActiveWorkspaceId($expectedId, $message = '') {
    $actualId = $this->workspaceModerationManager->getActiveWorkspace()->id();
    $this->assertEquals($expectedId, $actualId, $message);
  }

  /**
   * Asserts the title of the tested node.
   *
   * @param string $title
   *   The expected title.
   * @param string $message
   *   A message describing the assertion.
   */
  protected function assertNodeTitle($title, $message = '') {
    $this->assertEquals($title, $this->node->getTitle(), $message);
  }

  /**
   * Reloads the node.
   */
  protected function reloadNode() {
    $this->nodeStorage->resetCache();
    $this->node = $this->nodeStorage->load(1);
  }

  /**
   * Switches to a given workspace and resets static caches.
   *
   * @param string $id
   *   The workspace if.
   */
  protected function switchWorkspace($id) {
    // Switch to live first to clear the activeWorkspace property of the
    // core workspace manager.
    $this->workspaceModerationManager->switchToLive();

    /** @var \Drupal\workspaces\WorkspaceInterface $workspace */
    $workspace = Workspace::load($id);
    $this->workspaceModerationManager->setActiveWorkspace($workspace);
    $this->reloadNode();
  }

}
