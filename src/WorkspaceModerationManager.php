<?php

namespace Drupal\workspace_moderation;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workspaces\Entity\Workspace;
use Drupal\workspaces\WorkspaceInterface;
use Drupal\workspaces\WorkspaceManagerInterface;

// TODO: Add an interface with isModeratedWorkspace().

class WorkspaceModerationManager implements WorkspaceModerationManagerInterface {

  /**
   * The underlying manager service.
   *
   * @var \Drupal\workspaces\WorkspaceManagerInterface
   */
  protected $workspacesManager;

  protected $currentUser;

  /**
   * WorkspacesModerationManager constructor.
   *
   * @param \Drupal\workspaces\WorkspaceManagerInterface $workspaceManager
   *   The underlying manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   */
  public function __construct(WorkspaceManagerInterface $workspaceManager, AccountInterface $currentUser) {
    $this->workspacesManager = $workspaceManager;
    $this->currentUser = $currentUser;
  }

  public function getActiveWorkspace() {
    $workspace = $this->workspacesManager->getActiveWorkspace();

    if ($this->shouldUseShadowWorkspace($workspace)) {
      return $this->getShadowWorkspace($workspace);
    }

    return $workspace;
  }

  public function getShadowWorkspace(WorkspaceInterface $workspace) {
    $shadow = $this->getExistingShadowWorkspace($workspace);
    if (!$shadow) {
      $shadow = $this->createShadowWorkspace($workspace);
    }
    return $shadow;
  }

  public function shouldUseShadowWorkspace(WorkspaceInterface $workspace) {
    return !$this->isShadowWorkspace($workspace)
      && $this->isModeratedWorkspace($workspace)
      && $this->userCanModerateWorkspace($workspace);
  }

  public function isModeratedWorkspace(WorkspaceInterface $workspace) {
    // TODO: Find out which workspaces can be moderated.

    // TODO: Add a drupal_alter here.

    return TRUE;
  }

  public function userCanModerateWorkspace(WorkspaceInterface $workspace, $account = NULL) {
    if (!$account) {
      $account = $this->currentUser;
    }

    return $account->hasPermission('access moderated workspaces');
  }

  public function getExistingShadowWorkspace(WorkspaceInterface $workspace) {
    return Workspace::load($this->getShadowWorkspaceId($workspace));
  }

  public function getShadowWorkspaceId(WorkspaceInterface $workspace) {
    return $workspace->id() . '-moderated';
  }

  public function getShadowWorkspaceLabel(WorkspaceInterface $workspace) {
    return $workspace->label() . ' (moderated)';
  }

  public function createShadowWorkspace(WorkspaceInterface $workspace) {
    $shadow = Workspace::create([
      'id' => $this->getShadowWorkspaceId($workspace),
      'label' => $this->getShadowWorkspaceLabel($workspace),
      'parent' => $workspace->id(),
      'owner' => $workspace->getOwnerId(),
    ]);
    $shadow->save();

    return $shadow;
  }

  public function isShadowWorkspace(WorkspaceInterface $workspace) {
    return $this->isShadowWorkspaceId($workspace->id());
  }

  public function isShadowWorkspaceId(String $id) {
    // It is illegal to use a dash in a user-created workspace so the result
    // is reliable.
    return mb_substr($id, -10) == '-moderated';
  }

  public function isEntityTypeSupported(EntityTypeInterface $entity_type) {
    return $this->workspacesManager->isEntityTypeSupported($entity_type);
  }

  public function getSupportedEntityTypes() {
    return $this->workspacesManager->getSupportedEntityTypes();
  }

  public function hasActiveWorkspace() {
    return $this->workspacesManager->hasActiveWorkspace();
  }

  public function setActiveWorkspace(WorkspaceInterface $workspace) {
    return $this->workspacesManager->setActiveWorkspace($workspace);
  }

  public function switchToLive() {
    return $this->workspacesManager->switchToLive();
  }

  public function executeInWorkspace($workspace_id, callable $function) {
    return $this->workspacesManager->executeInWorkspace($workspace_id, $function);
  }

  public function executeOutsideWorkspace(callable $function) {
    return $this->workspacesManager->executeOutsideWorkspace($function);
  }

  public function shouldAlterOperations(EntityTypeInterface $entity_type) {
    return $this->workspacesManager->shouldAlterOperations($entity_type);
  }

  public function purgeDeletedWorkspacesBatch() {
    return $this->workspacesManager->purgeDeletedWorkspacesBatch();
  }

}
