<?php

namespace Drupal\workspace_moderation;

use Drupal\workspaces\WorkspaceInterface;
use Drupal\workspaces\WorkspaceManagerInterface;

interface WorkspaceModerationManagerInterface extends WorkspaceManagerInterface {

  public function getShadowWorkspace(WorkspaceInterface $workspace);

  public function shouldUseShadowWorkspace(WorkspaceInterface $workspace);

  public function isModeratedWorkspace(WorkspaceInterface $workspace);

  public function userCanModerateWorkspace(WorkspaceInterface $workspace, $account = NULL);

  public function getExistingShadowWorkspace(WorkspaceInterface $workspace);

  public function getShadowWorkspaceId(WorkspaceInterface $workspace);

  public function getShadowWorkspaceLabel(WorkspaceInterface $workspace);

  public function createShadowWorkspace(WorkspaceInterface $workspace);

  public function isShadowWorkspace(WorkspaceInterface $workspace);

  public function isShadowWorkspaceId(String $id);

}
