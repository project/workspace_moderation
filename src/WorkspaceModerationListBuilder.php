<?php

namespace Drupal\workspace_moderation;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\workspaces\WorkspaceListBuilder;

class WorkspaceModerationListBuilder extends WorkspaceListBuilder {

  /**
   * @var \Drupal\workspace_moderation\WorkspaceModerationManagerInterface
   */
  protected $workspaceManager;

  public function render() {
    $build = EntityListBuilder::render();
    $rows = &$build['table']['#rows'];
    $activeWorkspace = $this->workspaceManager->getActiveWorkspace();
    /** @var \Drupal\workspaces\WorkspaceInterface $parentWorkspace */
    $parentWorkspace =
      $this->workspaceManager->isShadowWorkspace($activeWorkspace)
      ? $activeWorkspace->parent->entity : NULL;

    // First, remove all the shadow workspaces. We don't want them to be shown
    // neither on the listing page nor in the off - canvas menu.
    foreach ($rows as $workspaceId => $row) {
      if ($this->workspaceManager->isShadowWorkspaceId($workspaceId)) {
        unset($rows[$workspaceId]);
      }
    }

    // The parent workspace cannot be activated, because technically it's
    // active already.
    if ($parentWorkspace && isset($rows[$parentWorkspace->id()])) {
      unset($rows[$parentWorkspace->id()]['data']['operations']['data']['#links']['activate']);
    }

    if ($this->isAjax()) {
      // If we're on a shadow workspace then we'd like to hide the original
      // workspace in the top off-canvas menu because we're on its shadow.
      if ($parentWorkspace && isset($rows[$parentWorkspace->id()])) {
        unset($rows[$parentWorkspace->id()]);
      }

      $this->offCanvasRender($build);
    }
    else {
      $build['#attached'] = [
        'library' => ['workspaces/drupal.workspaces.overview'],
      ];
    }
    return $build;
  }

}
